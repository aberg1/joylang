# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is Joy? ###

* From the Wikipedia page:

> The Joy programming language in computer science is a purely functional programming 
> language that was produced by Manfred von Thun of La Trobe University in Melbourne, 
> Australia. Joy is based on composition of functions rather than lambda calculus. It has
> turned out to have many similarities to Forth, due not to design but to a sort of parallel
> evolution and convergence. It was also inspired by the function-level programming style of 
> Backus's FP.

* I've decided that I want to try to resurrect this project after reading Backus's FP paper. I 
  suspect that there is lots of room for new work here, and plus I'm geneerally bored with all  
  the look-alike languages.
* I'm calling the downloade source version 1.0, because why not?

### Project status? ###

* Currently there is a makefile that will work on some POSIX system.
    * I'd like to port it to C# and be able to run it on .NET/MONO. That would give it a 
      better garbage collector (Bohem's isn't bad...), maybe a richer set of libraries, and the
      ability to run on Windows.
* Dependencies
    * Currently, POSIX
* How to run tests
    * I wish I knew. That will be one of the first things to figure out.
* Deployment instructions
    * If you manage to get it to build, it will drop a "joy" executable in the source 
      directory. Copy that somewhere.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Me!
* Other community or team contact
